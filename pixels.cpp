#include "pixels.hpp"
#include "hw_pins.h"

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#define PIXEL_OFFSET -5
#define ROT(i) (i<PIXEL_OFFSET? NUM_PIXELS + i - PIXEL_OFFSET : i - PIXEL_OFFSET) % NUM_PIXELS // compensate for the way I mounted...

int j=0; // for some fun light schemes!

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_PIXELS, LED1_PIN, NEO_GRB + NEO_KHZ800);

// Gamma correction improves appearance of midrange colors
const uint8_t PROGMEM gamma8[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,
    1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,  2,  3,  3,  3,  3,
    3,  3,  4,  4,  4,  4,  5,  5,  5,  5,  5,  6,  6,  6,  6,  7,
    7,  7,  8,  8,  8,  9,  9,  9, 10, 10, 10, 11, 11, 11, 12, 12,
   13, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20,
   20, 21, 21, 22, 22, 23, 24, 24, 25, 25, 26, 27, 27, 28, 29, 29,
   30, 31, 31, 32, 33, 34, 34, 35, 36, 37, 38, 38, 39, 40, 41, 42,
   42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
   58, 59, 60, 61, 62, 63, 64, 65, 66, 68, 69, 70, 71, 72, 73, 75,
   76, 77, 78, 80, 81, 82, 84, 85, 86, 88, 89, 90, 92, 93, 94, 96,
   97, 99,100,102,103,105,106,108,109,111,112,114,115,117,119,120,
  122,124,125,127,129,130,132,134,136,137,139,141,143,145,146,148,
  150,152,154,156,158,160,162,164,166,168,170,172,174,176,178,180,
  182,184,186,188,191,193,195,197,199,202,204,206,209,211,213,215,
  218,220,223,225,227,230,232,235,237,240,242,245,247,250,252,255
};

void pixels_set_rgb(uint8_t i, uint8_t r, uint8_t g, uint8_t b, bool gamma_correction)
{
  if(gamma_correction) pixels.setPixelColor(i, pgm_read_byte(&gamma8[r]), pgm_read_byte(&gamma8[g]), pgm_read_byte(&gamma8[b]));
  else pixels.setPixelColor(i, r, g, b);
}


void pixel_model_0_update(const sound_state_t * sound_state)
{
  // pure fft ()
  for(uint8_t i=0; i<NUM_PIXELS; i++)
  {
    pixels_set_rgb(ROT(i), sound_state->chroma_vector[i], 0, 0, 1);
  }
}


void pixel_model_1_update(const sound_state_t * sound_state)
{
  // VU meter + fft + peak tracker
  uint8_t chroma_vector_max = 0;
  uint8_t chroma_vector_max_index = 0;

  for(uint8_t i=0; i<NUM_PIXELS; i++)
  {    

    if(sound_state->chroma_vector[i] > chroma_vector_max )
    {
      chroma_vector_max = sound_state->chroma_vector[i];
      chroma_vector_max_index = i;
    }

    pixels_set_rgb(ROT(i), 
      sound_state->peak_to_peak_smooth, 
      sound_state->peak_to_peak, 
      sound_state->chroma_vector[i], 
      1);
  }

  pixels_set_rgb(ROT(chroma_vector_max_index), 0, 0, 255, 1);  

}

void pixel_model_2_update(const sound_state_t * sound_state)
{
  // VU meter + sample buffer
  for(uint8_t i=0; i<NUM_PIXELS; i++)
  {    
    pixels_set_rgb(ROT(i), 
      sound_state->peak_to_peak_smooth, 
      (sound_state->peak_to_peak_smooth >> 1) + (sound_state->sample_window[i] >> 1), 
      (255 - sound_state->peak_to_peak_smooth) >> 1,
      1);
  }
}

void pixel_model_99_update(const sound_state_t * sound_state)
{
  // opposing spinning lights + beacon pulse
  for(uint8_t i=0; i < NUM_PIXELS; i++)
  {
    pixels_set_rgb(ROT(i), 
      (i << 4) + (j >> 1), 
      (i << 4) - (j >> 2), 
      (256 - ((j >> 4) % 255)), 1);
  }
}

void pixels_init(pixel_state_t* pixel_state)
{
  pixels.begin();
  pixels_set_model(pixel_state, 2);
}

#define pixel_model( number ) case( number ): pixel_state->model_update = &pixel_model_ ## number ## _update; break

void pixels_set_model(pixel_state_t* pixel_state, uint8_t model)
{
  switch(model)
  {
    default:
    pixel_model(0);
    pixel_model(1);
    pixel_model(2);
    // ...
    pixel_model(99);
  }
}

void pixels_update(pixel_state_t* pixel_state, const sound_state_t * sound_state)
{

  pixel_state->model_update(sound_state);

  cli();
  pixels.show();
  sei();

  j++; 

}
