#ifndef _HW_PINS_H_
#define _HW_PINS_H_

#include <Arduino.h>

#define LED1_PIN       8
#define MIC_PIN        0

#define STATUS_LED_TOGGLE()	PORTB ^= B00100000
#define STATUS_LED_ON() 	PORTB |= B00100000
#define STATUS_LED_OFF()	PORTB &= B11011111

#define NUM_PIXELS 16

#endif