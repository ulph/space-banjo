/*
Control neo-pixles based on sound input
*/

#ifdef __AVR_ATtiny85__ // Trinket, Gemma, etc.
  #include <avr/power.h>
#endif

#include <Arduino.h> // todo http://www.baeyens.it/eclipse/Install.html instead!!

#include "pixels.hpp"
#include "sound.hpp"

bool status_pin = LOW;

sound_state_t sound_state;
pixel_state_t pixel_state;

#define log_and_run(what, arg) {Serial.println(#what); what( arg );} // :D ( will remove this later )

void setup() {
	pinMode(13, OUTPUT);
	digitalWrite(13, status_pin );
	Serial.begin(115200);

	#ifdef __AVR_ATtiny85__ // Trinket, Gemma, etc.
	  if(F_CPU == 16000000) clock_prescale_set(clock_div_1);
	  // Seed random number generator from an unused analog input:
	  randomSeed(analogRead(2));
	#else
	  randomSeed(analogRead(A0));
	#endif

	log_and_run(pixels_init, &pixel_state); // pixels_init( &pixel_state );
	log_and_run(sound_init, &sound_state); // sound_init( &sound_state );

}


void loop() {
  sound_update( &sound_state );
  pixels_update( &pixel_state, &sound_state );
}
