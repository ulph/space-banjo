#ifndef _PIXELS_HPP_
#define _PIXELS_HPP_

#include "sound.hpp"

typedef struct
{
	void (*model_update)(const sound_state_t *);
} pixel_state_t;

void pixels_init(pixel_state_t * /* OUT */);
void pixels_set_model(pixel_state_t* pixel_state /* OUT */, uint8_t model /* IN */);
void pixels_update(pixel_state_t * /* IN OUT */, const sound_state_t * sound_state /* IN */);

#endif // _PIXELS_H_