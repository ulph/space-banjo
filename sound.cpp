#include "sound.hpp"

#include "hw_pins.h"

#include <Arduino.h>

#define DEBUG_SOUND 1

#define AUDIOBUFFER_SIZE_N 128 // TODO; balance this with Fs
#define AUDIOBUFFER_COUNT 2

int audio_buffer[AUDIOBUFFER_COUNT][AUDIOBUFFER_SIZE_N];

//uint8_t fft_buffer[AUDIOBUFFER_SIZE_N];

volatile uint8_t free_buffer=0;
volatile uint8_t buffer_cleared=1;
volatile uint8_t buffer_swaps=0;
volatile uint8_t num_samples = 0;

#define AUDIOBUFFER_SWAP_BLINK_PERIOD 10
#define AUDIOBUFFER_SWAP_BLINK_HALF_PERIOD AUDIOBUFFER_SWAP_BLINK_PERIOD/2 // I sure hope this get optimized

#define SMOOTH_FACTOR_ATTACK 128
#define SMOOTH_FACTOR_DECAY 254

#define SMOOTH_FACTOR_CHROMA 128

#define CHROMA_VECTOR_SIZE NUM_PIXELS // no point in doing this differenty.

#define SAMPLE_WINDOW_SIZE NUM_PIXELS // no point in doing this differenty.
#define SAMPLE_WINDOW_F AUDIOBUFFER_SIZE_N / NUM_PIXELS

uint8_t chroma_vector[CHROMA_VECTOR_SIZE];

uint8_t sample_window[SAMPLE_WINDOW_SIZE];

void sound_init(sound_state_t * sound_state)
{
	sound_state->peak_to_peak = 0;
	sound_state->chroma_vector = chroma_vector;
	sound_state->sample_window = sample_window;

	cli();
  
	// So, CPU is 16 000 kHz. Compare register takes 13 cycles (quote?). Mic is up to 20 kHz.
	// if prescaler 32, Fs ~= 38.5 kHz (aka no risk for aliasing).  
	//
	// However, due to memory constraints we don't have enough buffer space. 
	// Which means we have to low pass filter digitally! :(
	//
	// OR, I build an RC cirtcuit ...

  	ADMUX=(1<<REFS0);
  	ADCSRA &= !((1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0)); // clear the prescalers
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (0 << ADPS0); // prescaler 128 ... NOT ideal as we get aliasing.
	ADCSRA |= (1 << ADATE); //enabble auto trigger
	ADCSRA |= (1 << ADIE); //enable interrupts when measurement complete
	ADCSRA |= (1 << ADEN); //enable ADC
	ADCSRA |= (1 << ADSC); //start ADC measurements

	sei();
}

void sound_update(sound_state_t * sound_state)
{
	if(buffer_cleared == 0)
	{
		int max = 0;
		int min = 0;
		uint8_t ptp = 0;

		// todo; rms?

		for (int i=0; i<AUDIOBUFFER_SIZE_N; i++)
		{
			int sample = audio_buffer[free_buffer][i];
			max = ( sample > max ) ? sample : max;
			min = ( sample < min ) ? sample : min;
		}

#if DEBUG_SOUND
		Serial.print(min);
		Serial.print(",");
		Serial.print(max);
#endif
		// clip to the range 0-255

		min = min < -511 ? -511 : min;

		ptp = (max < (-min) ? max : -min ) >> 1; // Keep the smallest number (get rid of spurious spikes). We care only about the one-sided value

#if DEBUG_SOUND
		Serial.print(",");
		Serial.print(ptp);
		Serial.println("");
#endif

		sound_state->peak_to_peak = ((SMOOTH_FACTOR_ATTACK * sound_state->peak_to_peak) + ((256 - SMOOTH_FACTOR_ATTACK) * ptp)) >> 8;

		sound_state->peak_to_peak_smooth = ((SMOOTH_FACTOR_DECAY * sound_state->peak_to_peak_smooth) + ((256 - SMOOTH_FACTOR_DECAY) * sound_state->peak_to_peak)) >> 8;
		sound_state->peak_to_peak_smooth = sound_state->peak_to_peak > sound_state->peak_to_peak_smooth ? sound_state->peak_to_peak : sound_state->peak_to_peak_smooth;

		// todo; refactor to move to loop above ... ?
		for (uint8_t i=0; i < SAMPLE_WINDOW_SIZE; i++)
		{
			int sample = 0;
			for(uint8_t j=0; j < SAMPLE_WINDOW_F; j++)
			{
				sample += audio_buffer[free_buffer][(i*SAMPLE_WINDOW_F + j)]; 
			}
			sample /= SAMPLE_WINDOW_F;
			sound_state->sample_window[i] = (uint8_t)( sample > 0 ? sample : 0); // half wave rectifier
		}

		for (uint8_t i=0; i < CHROMA_VECTOR_SIZE; i++)
		{
			// todo - fft calc;
			uint8_t bin = 0; 
			sound_state->chroma_vector[i] = ((SMOOTH_FACTOR_CHROMA * sound_state->chroma_vector[i]) + ((256 - SMOOTH_FACTOR_CHROMA) * bin)) >> 8;
		}

		buffer_cleared = 1;	
	}
}

int sample_lp = 0;

ISR(ADC_vect)
{
	uint8_t fill_buffer = free_buffer?1:0;
	int sample = ADCL;
	sample += (ADCH << 8);

	// correct for me connecting to the 3.3v rather than 5v ... :(
	sample -= ((512 * 33) / 50 );
	sample *= 50; // should really be 50, but if decreasing slightly the extra headroom of the positive side is used.
	sample /= 33;
	sample = (sample > 511) ? 511 : ( sample < -512 ? -512 : sample);
	// end correction

 	audio_buffer[fill_buffer][num_samples] = sample;

  	if(num_samples == (AUDIOBUFFER_SIZE_N-1))
  	{
  		if(buffer_cleared)
  		{
	  		free_buffer = fill_buffer;
	  		buffer_cleared = 0;

	  		// blinky
	  		buffer_swaps %= AUDIOBUFFER_SWAP_BLINK_HALF_PERIOD;
	  		if(0 == buffer_swaps) STATUS_LED_TOGGLE();	
			buffer_swaps++;
		}
		else
		{
			STATUS_LED_ON();
		}
  	}
  	num_samples ++;
	num_samples %= AUDIOBUFFER_SIZE_N;
}