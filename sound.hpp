#ifndef _SOUND_HPP_
#define _SOUND_HPP_

#include <Arduino.h>

typedef struct
{
	uint8_t peak_to_peak;
	uint8_t peak_to_peak_smooth; // slower decay
	uint8_t* chroma_vector;
	uint8_t* sample_window;
} sound_state_t;

void sound_init(sound_state_t * /* OUT */);
void sound_update(sound_state_t * /* OUT */);

#endif // _SOUND_HPP_